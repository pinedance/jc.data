var jichengSearchEngine = {
  conf: {
    bookListPages: ['index.html'],
    searchConfirmBookNumberThreshold: 20,
    punctuationList: ':;.,-–—‒_(){}[]!\'"+=﹐，。、．·‧？！：﹕︰；﹔…～＄％＠＆＃＊「」『』〝〞‘’“”〈〉《》＜＞﹤﹥﹁﹂﹃﹄︿﹀︽︾',
    exclude: ['style', 'script', 'noscript', 'form', 'template', 'frame', 'iframe'],
  },

  /**
   * Main
   */
  async init() {
    // 初始化 GUI
    this.filterBooksValidate();
    this.searchValidate();

    const header = document.querySelector('#search-wrapper header');
    header.removeAttribute('class');
    header.textContent = `正在載入檢索引擎...`;

    // 載入 URL 參數
    {
      const params = new URLSearchParams (location.search.replace('?', ''));
      if (params.has('index')) {
        this.conf.bookListPages = params.getAll('index');
      }
    }

    // 載入書籍列表
    try {
      const wrapper = document.querySelector('#search-panel-books');

      for (let url of this.conf.bookListPages) {
        let doc;
        try {
          doc = (await jicheng.xhr({
            url,
            responseType: 'document',
            overrideMimeType: 'text/html',
          })).response;
        } catch (ex) {
          throw new Error(`network request for '${url}' failed.`);
        }

        if (!doc) {
          throw new Error(`document at '${url}' is null.`);
        }

        let subWrapper = wrapper;
        if (this.conf.bookListPages.length > 1) {
          subWrapper = wrapper.appendChild(document.createElement('optgroup'));
          subWrapper.label = url;
        }

        Array.prototype.forEach.call(doc.querySelectorAll('main > ol li > a[href]'), (elem) => {
          // 處理叢書 A·B 的情況
          let name = elem.textContent;
          let parent = elem.closest('ol').closest('ol > li');
          while (parent) {
            const a = parent.querySelector('a');
            if (a && a.parentNode === parent) {
              const prefix = a.textContent;
              if (prefix) {
                name = prefix + '·' + name;
              }
            }
            parent = parent.closest('ol').closest('ol > li');
          }

          const opt = document.createElement('option');
          opt.value = elem.href;
          opt.textContent = name;
          subWrapper.appendChild(opt);
        });
      }

      header.textContent = `本檢索引擎會把選取的典籍即時下載至本機做精確條件比對，可能佔用較多網路流量及本機資源。`;

      const panel = document.querySelector('#search-panel');
      panel.hidden = false;
    } catch (ex) {
      header.className = 'error';
      header.textContent = `錯誤：無法載入典籍列表: ${ex.message}`;
    }
  },

  filterBooks() {
    let query;
    try {
      query = this.parseKeyword({
        keyword: document.querySelector('#search-panel-filterbook-keyword').value,
        useSynonyms: document.querySelector('#search-panel-synonyms').checked,
        ignoreCase: document.querySelector('#search-panel-case').checked,
        useRegex: document.querySelector('#search-panel-regex').checked,
      });
    } catch (ex) {
      console.error(ex);
      return;
    }
    console.log(`篩選: ${query.keyword} => ${query.query}`);

    const booksWrapper = document.querySelector('#search-panel-books');
    Array.prototype.forEach.call(booksWrapper.querySelectorAll('option'), (elem) => {
      switch (query.type) {
        case 'eval': {
          const text = elem.textContent;
          if (!eval(query.query)) {
            elem.hidden = true;
            elem.disabled = true;
            elem.selected = false;
          };
          break;
        }
        case 'regex': 
        default: {
          if (!query.query.test(elem.textContent)) {
            elem.hidden = true;
            elem.disabled = true;
            elem.selected = false;
          }
          break;
        }
      }
    });
  },

  filterBooksByResults() {
    const filter = new Set(
      Array.prototype.map.call(
        document.querySelectorAll('#search-results > details > summary > a:first-child'),
        el => el.textContent
      )
    );

    if (filter.size === 0) {
      console.error(`目前沒有檢索結果，篩選典籍中止。`);
      return;
    }

    const booksWrapper = document.querySelector('#search-panel-books');
    Array.prototype.forEach.call(booksWrapper.querySelectorAll('option'), (elem) => {
      if (!filter.has(elem.textContent)) {
        elem.hidden = true;
        elem.disabled = true;
        elem.selected = false;
      }
    });
  },

  filterBooksValidate() {
    const elem = document.querySelector('#search-panel-filterbook-keyword');
    const useRegex = document.querySelector('#search-panel-regex').checked;
    if (useRegex) {
      const keyword = elem.value;
      try {
        new RegExp(keyword);
      } catch (ex) {
        elem.pattern = `(?!${jicheng.escapeRegExp(keyword)}$).*`;
      }
      elem.title = '請輸入有效的正規表示式';
      return;
    }
    elem.removeAttribute('pattern');
    elem.removeAttribute('title');
  },

  filterBooksSelectAll() {
    const booksWrapper = document.querySelector('#search-panel-books');
    Array.prototype.forEach.call(booksWrapper.querySelectorAll('option:not([hidden])'), (elem) => {
      elem.selected = true;
    });
  },

  filterBooksReset() {
    const booksWrapper = document.querySelector('#search-panel-books');
    Array.prototype.forEach.call(booksWrapper.querySelectorAll('option'), (elem) => {
      elem.hidden = false;
      elem.disabled = false;
      elem.selected = false;
    });
  },

  filterBooksView() {
    const booksWrapper = document.querySelector('#search-panel-books');
    const books = Array.prototype.filter.call(
      booksWrapper.querySelectorAll('option'),
      x => x.selected
    );

    // 選取的典籍太多時，向使用者確認
    if (books.length > 1) {
      if (!confirm(`確定要開啟 ${books.length} 部典籍檢視？`)) {
        return;
      }
    }

    books.forEach((book) => {
      window.open(book.value);
    });
  },

  async search() {
    const startTime = Date.now();

    // 將 #search-results 換成另一個元素，使之前的檢索中斷
    const resultWrapperOrig = document.querySelector('#search-results');
    const resultWrapper = resultWrapperOrig.cloneNode(false);
    resultWrapperOrig.parentNode.replaceChild(resultWrapper, resultWrapperOrig);

    // 建立檢索結果主標
    const header = resultWrapper.appendChild(document.createElement('header'));
    header.textContent = `檢索準備中...`;

    // 建立要檢索的書籍列表
    // 若無書則中止
    let books;
    {
      const booksWrapper = document.querySelector('#search-panel-books');

      books = Array.prototype.filter.call(
        booksWrapper.querySelectorAll('option'),
        elem => elem.selected,
      ).map(elem => ({
        name: elem.textContent,
        url: elem.value,
      }));

      if (books.length <= 0) {
        header.className = 'error';
        header.textContent = `錯誤：請選取要檢索的典籍。`;
        return;
      }
    }

    // 建立比對及標示關鍵詞用的 RegExp
    // 若發生錯誤則中止
    const keyword = document.querySelector('#search-panel-keyword').value;

    if (!keyword) {
      header.className = 'error';
      header.textContent = `錯誤：請輸入關鍵詞。`;
      return;
    }

    let query;
    try {
      query = this.parseKeyword({
        keyword,
        useSynonyms: document.querySelector('#search-panel-synonyms').checked,
        ignoreCase: document.querySelector('#search-panel-case').checked,
        useRegex: document.querySelector('#search-panel-regex').checked,
      });
    } catch (ex) {
      header.className = 'error';
      header.textContent = `錯誤：${ex.message}`;
      return;
    }
    console.log(`檢索: ${query.keyword} => ${query.query}`);

    // 選取的典籍太多時，向使用者確認
    if (books.length >= this.conf.searchConfirmBookNumberThreshold) {
      if (!confirm(`檢索 ${books.length} 部典籍可能佔用大量網路流量及系統資源，確定要繼續嗎？`)) {
        header.textContent = `檢索已取消。`;
        return;
      }
    }

    // 顯示開始檢索的訊息
    header.textContent = '';
    header.appendChild(document.createTextNode(`正在檢索第 `));
    const bookCountElem = header.appendChild(document.createElement('span'));
    bookCountElem.textContent = '0';
    header.appendChild(document.createTextNode(`/`));
    const bookTotalElem = header.appendChild(document.createElement('span'));
    bookTotalElem.textContent = books.length;
    header.appendChild(document.createTextNode(` 部典籍，已找到 `));
    const resultCountElem = header.appendChild(document.createElement('span'));
    resultCountElem.textContent = '0';
    header.appendChild(document.createTextNode(` 筆結果...`));

    // 依次載入各頁面做檢索
    const handleSearchHit = (elem, cbMakeTitle) => {
      let text = elem.textContent;

      // 若設定不比對標點，則移除比對字串中的標點
      if (!usePunctuation) {
        text = text.replace(rPunctuations, '');
      }

      switch (query.type) {
        case 'eval':
          if (!eval(query.query)) { return false; }
          break;
        case 'regex':
          if (!query.query.test(text)) { return false; }
          break;
      }

      const item = document.createElement('details');
      item.open = expandAll;
      const header = item.appendChild(document.createElement('summary'));
      cbMakeTitle(header);

      const div = item.appendChild(document.createElement('div'));
      if (showSummary) {
        let snippets = [];
        let offset = 0;
        let end = 0;
        let len = text.length;

        // 建立比對關鍵詞的 RegExp
        const re1 = new RegExp(`(${query.highlightRegExp.source})`, query.highlightRegExp.flags);
        const re2 = new RegExp(`${re1}.{0,75}(?!\\1)${re1}`, query.highlightRegExp.flags);
        const re3 = new RegExp(`${re1}.{0,45}(?!\\1)${re1}.{0,45}(?!\\1)(?!\\2)${re1}`, query.highlightRegExp.flags);

        for (let cnt = 4; cnt--;) {
          re1.lastIndex = re2.lastIndex = re3.lastIndex = offset;
          const match = re3.exec(text) || re2.exec(text) || re1.exec(text);
          if (!match) { break; }

          const str = match[0];
          const strlen = str.length;
          const idx = match.index;

          // 建立前後文，取匹配字串前後共 100 字
          // 若一側不足 50 字則加長另一側
          let pre = Math.min(idx - offset, 100);
          let post = Math.min(len - idx - strlen, 100);

          if (pre > 50 && post > 50) {
            pre = post = 50;
          } else if (pre > 50) {
            pre = Math.min(pre, 100 - post);
          } else if (post > 50) {
            post = Math.min(post, 100 - pre);
          } else if (offset === 0) {
            // 兩側都不足 50 字，直接使用整個字串，略過複雜的全文片段演算法
            snippets = [text];
            break;
          }

          // 計算起始點及結束點。如有重疊則加到上一個片段
          const start = idx - pre;
          const append = (start < end) ? end : false;  // 開始點在上一片段結尾前，則加到上一片段
          end = idx + strlen + post;  // 現在將結束點設為此片段後

          if (append) {
            snippets[snippets.length - 1] += text.substring(append, end);
          } else {
            snippets.push(text.substring(start, end));
          }

          // 設定 offset 以決定比對下一個關鍵詞的起始點
          offset = idx + strlen;
        }

        div.textContent = snippets.join('…');
        div.className = 'snippets';
      } else {
        switch (elem.nodeType) {
          case 1:
            div.innerHTML = elem.innerHTML;
            break;
          case 11:
            div.appendChild(elem);
            break;
          default:
            console.error(new Error(`Unsupported node type: ${elem.nodeName}`));
            break;
        }

        // 若設定不比對標點，則移除輸出全文的標點
        if (!usePunctuation) {
          new Mark(div).markRegExp(rPunctuations, {
            each: n => n.remove(),
          });
          div.normalize();
        }
      }

      // 標示關鍵詞
      new Mark(div).markRegExp(query.highlightRegExp, {
        acrossElements: true,
      });

      resultWrapper.appendChild(item);
      resultCountElem.textContent = parseInt(resultCountElem.textContent, 10) + 1;
    };

    const handleSearchHitSource = (elem, cbMakeTitle) => {
      let text = elem.innerHTML.replace(/^\n/, '');
      let hits = 0;

      // 若設定不比對標點，則移除比對字串中的標點
      if (!usePunctuation) {
        text = text.replace(rPunctuations, '');
      }

      switch (query.type) {
        case 'eval':
          if (!eval(query.query)) { return false; }
          break;
        case 'regex':
          if (!query.query.test(text)) { return false; }
          break;
      }

      const item = document.createElement('details');
      item.open = expandAll;
      const header = item.appendChild(document.createElement('summary'));
      cbMakeTitle(header);

      const div = item.appendChild(document.createElement('div'));
      const pre = div.appendChild(document.createElement('pre'));
      pre.className = 'source';
      if (showSummary) {
        div.className = 'snippets';

        // 標示關鍵詞
        const dummy = document.createElement('div');
        dummy.textContent = 'test';
        new Mark(dummy).markRegExp(/test/, {
          acrossElements: false,
        });
        const dummyHtml = dummy.innerHTML;
        const regex = /<[^>]*?>/ig;
        regex.exec(dummyHtml);
        const tagStart = RegExp.lastMatch;
        regex.exec(dummyHtml);
        const tagEnd = RegExp.lastMatch;
        const regex1 = new RegExp(jicheng.escapeRegExp(tagStart) + '|' + jicheng.escapeRegExp(tagEnd));
        const regex2 = new RegExp('^(?:(?!' + jicheng.escapeRegExp(tagStart) + ').)*?' + jicheng.escapeRegExp(tagEnd));
        const regex3 = new RegExp(jicheng.escapeRegExp(tagStart) + '(?:(?!' + jicheng.escapeRegExp(tagEnd) + ').)*?$');

        const dummyElem = document.createElement('pre');
        dummyElem.textContent = text;
        new Mark(dummyElem).markRegExp(query.highlightRegExp, {
          acrossElements: false,
          each: (elem) => {
            hits += 1;
          },
        });

        // 輸出含有關鍵詞的行
        dummyElem.innerHTML.split('\n').forEach((line, index) => {
          if (line.match(regex1)) {
            if (line.match(regex2)) {
              line = tagStart + line;
            }
            if (line.match(regex3)) {
              line = line + tagEnd;
            }
            const innerDiv = pre.appendChild(document.createElement('div'));
            innerDiv.innerHTML = line;
            const innerB = innerDiv.insertBefore(document.createElement('b'), innerDiv.firstChild);
            innerB.textContent = `行${index + 1}: `;
          }
        });
      } else {
        pre.textContent = text;

        // 標示關鍵詞
        new Mark(div).markRegExp(query.highlightRegExp, {
          acrossElements: false,
          each: (elem) => {
            hits += 1;
          },
        });
      }

      resultWrapper.appendChild(item);
      resultCountElem.textContent = parseInt(resultCountElem.textContent, 10) + hits;
    };

    const rPunctuations = new RegExp('(?:' + this.conf.punctuationList
      .split(/(?![\uDC00-\uDFFF])/)
      .map(x => jicheng.escapeRegExp(x))
      .join('|') + ')+', 'ig');
    const searchUnit = document.querySelector('#search-panel-unit').value;
    const showSummary = document.querySelector('#search-panel-showSummary').checked;
    const expandAll = document.querySelector('#search-panel-expandAll').checked;
    const usePunctuation = document.querySelector('#search-panel-usePunctuation').checked;

    // 建立不檢索元素的 selector 列表
    let removeElemSelector = this.conf.exclude.slice();
    let removeWrapperSelector = [];
    {
      if (!document.querySelector('#search-panel-useAncient').checked) {
        removeElemSelector.push('del');
        removeWrapperSelector.push('[class~="古版"]');
      } else {
        removeElemSelector.push('ins');
        removeWrapperSelector.push('[class~="今版"]');
      }

      if (!document.querySelector('#search-panel-useZhu').checked) {
        removeElemSelector.push('span.註');
      }

      if (!document.querySelector('#search-panel-useShu').checked) {
        removeElemSelector.push('span.疏');
      }

      if (!document.querySelector('#search-panel-useJiao').checked) {
        removeElemSelector.push('span.校');
      }

      if (!document.querySelector('#search-panel-useNumbering').checked) {
        removeElemSelector.push('span.編號');
      }
    }
    removeElemSelector = removeElemSelector.join(', ');
    removeWrapperSelector = removeWrapperSelector.join(', ');

    for (let book of books) {
      const {name, url} = book;

      try {
        // 檢查檢索是否已中斷
        if (!resultWrapper.parentNode) { throw new Error('使用者中止了檢索。'); }

        bookCountElem.textContent = parseInt(bookCountElem.textContent, 10) + 1;

        let doc;
        try {
          doc = (await jicheng.xhr({
            url,
            responseType: 'document',
            overrideMimeType: 'text/html',
          })).response;
          if (!doc) {
            throw new Error(`document for '${name}' is null.`);
          }
        } catch (ex) {
          const details = resultWrapper.appendChild(document.createElement('details'));
          const summary = details.appendChild(document.createElement('summary'));
          summary.className = 'error';
          summary.textContent = `錯誤：無法載入典籍「${name}」`;
          throw new Error(`unable to retrieve document for '${name}'.`);
        }

        // 根元素
        const mainElem = doc.querySelector('main');

        // 為參考點元素建立對照資訊
        const mapRefUpperRef = new Map(); // e.g. h2 -> h1; p -> h3
        const mapRefXpath = new Map();
        if (searchUnit !== 'book') {
          const mapLevelHeader = [];

          let selector = 'h1, h2, h3, h4, h5, h6';
          switch (searchUnit) {
            case 'subchapter':
              selector += ', hr[class~="分段"]';
              break;
            case 'paragraph':
              selector += ', p, div[class~="段落"]';
              break;
          }

          await jicheng.forEachAsync(
            mainElem.querySelectorAll(selector),
            (elem, index) => {
              if (!elem.closest('main')) { return; }

              // 找出此節點上一層參考節點
              {
                let parent = null;
                if (/^h(\d+)$/i.test(elem.nodeName)) {
                  const level = parseInt(RegExp.$1, 10);
                  mapLevelHeader[level - 1] = elem;
                  for (let i = mapLevelHeader.length - 1; i > level - 1; i--) {
                    mapLevelHeader.pop();
                  }

                  if (level - 1 > 0) {
                    parent = mapLevelHeader[level - 2] || null;
                  }
                } else {
                  if (mapLevelHeader.length) {
                    parent = mapLevelHeader[mapLevelHeader.length - 1];
                  }
                }
                mapRefUpperRef.set(elem, parent);
              }

              // 算出此節點的 XPath
              {
                const segments = []; 
                let current = elem;
                while (current) {
                  if (current === mainElem) { break; }

                  let i = 1;
                  let sibling = current.previousElementSibling;
                  while (sibling) {
                    if (sibling.nodeName === current.nodeName) {
                      if (mapRefXpath.has(sibling)) {
                        i += parseInt(mapRefXpath.get(sibling).match(/(\d+)[^\d]*?$/)[1], 10);
                        break;
                      }
                      i++;
                    }
                    sibling = sibling.previousElementSibling;
                  }
                  segments.unshift(`${current.nodeName.toLowerCase()}[${i}]`);

                  current = current.parentNode;
                }
                const xpath = segments.join('/');
                mapRefXpath.set(elem, xpath);
              }
            });
        }

        // 移除不檢索的元素
        {
          await jicheng.forEachAsync(
            mainElem.querySelectorAll(removeElemSelector),
            (elem) => {
              elem.remove();
            }
          );

          await jicheng.forEachAsync(
            mainElem.querySelectorAll(removeWrapperSelector),
            (elem) => {
              while (elem.firstChild) {
                elem.parentNode.insertBefore(elem.firstChild, elem);
              }
              elem.remove();
            }
          );
        }

        // 針對指定的搜尋單位做比對，並輸出結果
        switch (searchUnit) {
          case 'book': {
            handleSearchHit(mainElem, (header) => {
              const anchor = header.appendChild(document.createElement('a'));
              anchor.href = url
                  + '#hl=' + encodeURIComponent(query.highlightRegExp.toString())
                  + '&useAncient=false';
              anchor.target = '_blank';
              anchor.textContent = name;
            });

            break;
          }

          case 'chapter':
          case 'subchapter':
          case 'paragraph': {
            const mapOrigClone = new Map();
            const mapFragmentRef = new Map();

            // 將每個 h# 至下一個 h# 之間的範圍建立為一個 fragment
            const fragments = [document.createDocumentFragment()];
            mapFragmentRef.set(fragments[fragments.length - 1], null);

            const nodes = {
              [Symbol.iterator]: () => {
                const nodeIterator = document.createNodeIterator(mainElem, NodeFilter.SHOW_ALL);
                nodeIterator.nextNode(); // 略過 mainElem
                return {
                  next() {
                    const node = nodeIterator.nextNode();
                    return {value: node, done: !node};
                  }
                };
              }
            };
            await jicheng.forEachAsync(nodes, (elem) => {
              mapOrigClone.set(elem, elem.cloneNode(false));

              if (mapRefUpperRef.has(elem)) {
                // elem 為參照元素（h#, p, ...）
                // 建立新 fragment
                // 為 elem 及其祖節點建立複本並加入
                const fragment = document.createDocumentFragment();
                fragments.push(fragment);
                mapFragmentRef.set(fragment, elem);

                let current = elem;
                let parent = current.parentNode;
                while (parent) {
                  if (parent === mainElem) { break; }
                  mapOrigClone.set(parent, parent.cloneNode(false));
                  mapOrigClone.get(parent).appendChild(mapOrigClone.get(current));
                  current = parent;
                  parent = parent.parentNode;
                }
                fragment.appendChild(mapOrigClone.get(current));
              } else {
                // elem 非參照元素
                // 建立複本並加入其 parent 的複本（或目前 fragment）
                const parent = elem.parentNode;
                if (parent === mainElem) {
                  fragments[fragments.length - 1].appendChild(mapOrigClone.get(elem));
                } else {
                  mapOrigClone.get(parent).appendChild(mapOrigClone.get(elem));
                }
              }
            });

            // 對每一個 fragment 做檢索
            await jicheng.forEachAsync(fragments, (fragment, index) => {
              handleSearchHit(fragment, (header) => {
                const breadcrumb = document.createDocumentFragment();
                const sep = ' » ';

                let elem = mapFragmentRef.get(fragment);
                while (elem !== undefined) {
                  if (elem === null) {
                    // 首節
                    break;
                  }

                  const anchor = breadcrumb.insertBefore(document.createElement('a'), breadcrumb.firstChild);
                  anchor.href = url
                      + '#t=' + encodeURIComponent(mapRefXpath.get(elem))
                      + '&hl=' + encodeURIComponent(query.highlightRegExp.toString())
                      + '&useAncient=false';
                  anchor.target = '_blank';

                  if (/^h(\d+)$/i.test(elem.nodeName)) {
                    anchor.textContent = elem.textContent.trim();
                  } else {
                    anchor.textContent = '#' + (index + 1);
                  }

                  breadcrumb.insertBefore(document.createTextNode(sep), breadcrumb.firstChild);
                  elem = mapRefUpperRef.get(elem);
                }

                // 加入第一層
                {
                  const anchor = breadcrumb.insertBefore(document.createElement('a'), breadcrumb.firstChild);
                  anchor.href = url
                      + '#hl=' + encodeURIComponent(query.highlightRegExp.toString())
                      + '&useAncient=false';
                  anchor.target = '_blank';
                  anchor.textContent = name;
                }

                header.appendChild(breadcrumb);
              });
            });

            break;
          }

          case 'source': {
            handleSearchHitSource(mainElem, (header) => {
              const anchor = header.appendChild(document.createElement('a'));
              anchor.href = url
                  + '#useAncient=false';
              anchor.target = '_blank';
              anchor.textContent = name;
            });

            break;
          }
        }
      } catch (ex) {
        console.error(ex);
      }
    }

    // 檢索完成，更新主標
    const time = (Date.now() - startTime) / 1000;
    header.textContent = `從 ${parseInt(bookCountElem.textContent, 10)} 部典籍中找到 ${parseInt(resultCountElem.textContent, 10)} 筆結果，費時 ${time} 秒。`;
  },

  searchValidate() {
    const elem = document.querySelector('#search-panel-keyword');
    const useRegex = document.querySelector('#search-panel-regex').checked;

    if (useRegex) {
      const keyword = elem.value;
      try {
        new RegExp(keyword);
      } catch (ex) {
        elem.pattern = `(?!${jicheng.escapeRegExp(keyword)}\$).*`;
      }
      elem.title = '請輸入有效的正規表示式';
      return;
    }
    elem.removeAttribute('pattern');
    elem.removeAttribute('title');
  },

  getSynonymList() {
    if (!this.getSynonymList.synonymList) {
      // 準備異體字表及轉換函數
      const synonyms = jicheng.synonyms;
      const synonymsEscaped = {};
      let synonymsEscapedMaxLen = -Infinity;

      Object.keys(synonyms).forEach((key) => {
        const keys = synonyms[key].slice();
        keys.unshift(key);
        key = jicheng.escapeRegExp(key);
        synonymsEscapedMaxLen = Math.max(synonymsEscapedMaxLen, key.length);
        synonymsEscaped[key] = keys.map(jicheng.escapeRegExp);
      });

      this.getSynonymList.synonymMaxLen = synonymsEscapedMaxLen;
      this.getSynonymList.synonymList = synonymsEscaped;
    }

    return this.getSynonymList.synonymList;
  },

  /**
   * 將 regexStr 中的字串取代為可比對任一異體字
   *
   * - 略過 [...]，因 [A] 轉為 [(?:A1|A2)] 會造成語義錯誤
   * - 略過 \xhh, \uhhhh
   */
  propagateSynonyms(regexStr) {
    const synonymList = this.getSynonymList();
    const synonymMaxLen = this.getSynonymList.synonymMaxLen;

    const replaceWithSynonymList = (str) => {
      const output = [];
      for (let i = 0, I = str.length; i < I; i++) {
        let s = str.substr(i, synonymMaxLen);
        for (let j = synonymMaxLen; j >= 1; j--) {
          s = s.substr(0, j);
          let v = synonymList[s];
          if (v) {
            output.push(`(?:${v.join('|')})`);
            s = null;
            i += j - 1;
            break;
          }
        }
        if (s) { output.push(s); }
      }
      return output.join('');
    };

    const fixerRegex = /\\x[0-9A-Fa-f]{2}|\\u[0-9A-Fa-f]{4}|(\\.)|\[(?:\\.|[^\\\]])+\]/g;
    const result = [];
    let lastIndex = 0;
    while (fixerRegex.test(regexStr)) {
      if (RegExp.$1) { continue; }
      const delta = regexStr.substring(lastIndex, fixerRegex.lastIndex - RegExp.lastMatch.length);
      result.push(replaceWithSynonymList(delta));
      result.push(RegExp.lastMatch);
      lastIndex = fixerRegex.lastIndex;
    }
    const delta = regexStr.substring(lastIndex);
    result.push(replaceWithSynonymList(delta));
    return result.join('');
  },

  parseKeyword({keyword, useSynonyms, ignoreCase, useRegex}) {
    const regexFlag = ignoreCase ? 'm' : 'im';

    if (!useRegex) {
      const fixerRegex = /((?:^|\s+)[Aa][Nn][Dd](?=\s|$))|(\s*\||(?:^|\s+)[Oo][Rr](?=\s|$))|(\s*\()|(\s*\))|(\s+)|"((?:""|[^"])*)"/g;

      let q = [], m, keys = [], i = 0, delta, neg = false;
      while ((m = fixerRegex.exec(keyword))) {
        delta = keyword.substring(i, m.index).trim();
        while (delta[0] === '-') {
          q.push('!');
          neg = !neg;
          delta = delta.slice(1);
        }
        if (delta.length) {
          let key = jicheng.escapeRegExp(delta);
          if (useSynonyms) { key = this.propagateSynonyms(key); }
          neg ? neg = false : keys.push(key);
          q.push('/' + key + '/' + regexFlag + '.test(text)');
        }

        if (m[1] !== undefined) {
          q.push('&&');
        } else if (m[2] !== undefined) {
          q.push('||');
        } else if (m[3] !== undefined) {
          q.push('(');
        } else if (m[4] !== undefined) {
          q.push(')');
        } else if (m[5] !== undefined) {
          // do nothing
        } else if (m[6] !== undefined) {
          let key = jicheng.escapeRegExp(m[6].replace(/""/g, '"'));
          if (useSynonyms) { key = this.propagateSynonyms(key); }
          neg ? neg = false : keys.push(key);
          q.push('/' + key + '/' + regexFlag + '.test(text)');
        }

        i = fixerRegex.lastIndex;
      }

      delta = keyword.substring(i).trim();
      while (delta[0] === '-') {
        q.push('!');
        neg = !neg;
        delta = delta.slice(1);
      }
      if (delta.length) {
        let key = jicheng.escapeRegExp(delta);
        if (useSynonyms) { key = this.propagateSynonyms(key); }
        neg ? neg = false : keys.push(key);
        q.push('/' + key + '/' + regexFlag + '.test(text)');
      }

      for (i = 0; i < q.length - 1; i++) {
        if (/^[\/)]/.test(q[i]) && /^[\/(!]/.test(q[i+1])) {
          q[i] += ' &&';
        }
      }

      q = q.join(' ');

      // 驗證語法是否正確
      try {
        const text = '';
        eval(q);
      } catch(ex) {
        console.error(`Bad query: ${keyword} => ${q}`);
        throw new Error(`檢索條件「${keyword}」語法有誤。`);
      }

      return {
        type: 'eval',
        keyword,
        query: q,
        highlightRegExp: new RegExp(keys.join('|'), regexFlag + 'g'),
      };
    } else {
      let key = keyword;
      if (useSynonyms) {
        key = this.propagateSynonyms(key);
      }

      // 驗證語法是否正確
      let query;
      let highlightRegExp;
      try {
        query = new RegExp(key, regexFlag);
        highlightRegExp = new RegExp(key, regexFlag + 'g');
      } catch(ex) {
        console.error(`Bad query: ${keyword} => /${key}/${regexFlag}`);
        throw new Error(`正規表示式「${keyword}」語法有誤。`);
      }

      return {
        type: 'regex',
        keyword,
        query,
        highlightRegExp,
      };
    }
  },
};

document.querySelector('#search-panel-filterbook-keyword').addEventListener('input', (event) => {
  jichengSearchEngine.filterBooksValidate();
});

document.querySelector('#search-panel-filterbook-selectAll').addEventListener('click', (event) => {
  jichengSearchEngine.filterBooksSelectAll();
});

document.querySelector('#search-panel-filterbook-reset').addEventListener('click', (event) => {
  jichengSearchEngine.filterBooksReset();
});

document.querySelector('#search-panel-form-range').addEventListener('submit', (event) => {
  event.preventDefault();
  jichengSearchEngine.filterBooks();
});

document.querySelector('#search-panel-keyword').addEventListener('input', (event) => {
  jichengSearchEngine.searchValidate();
});

document.querySelector('#search-panel-regex').addEventListener('change', (event) => {
  jichengSearchEngine.filterBooksValidate();
  jichengSearchEngine.searchValidate();
});

document.querySelector('#search-panel-punctuationList').addEventListener('click', (event) => {
  event.preventDefault();
  const result = prompt('請設定要視為標點符號的字元：', jichengSearchEngine.conf.punctuationList);
  if (result === null) { return; }  // cancel
  jichengSearchEngine.conf.punctuationList = result;
});

document.querySelector('#search-panel-form-settings').addEventListener('submit', (event) => {
  event.preventDefault();
  jichengSearchEngine.search();
});

document.querySelector('#search-panel-filterBookByResults').addEventListener('click', (event) => {
  jichengSearchEngine.filterBooksByResults();
});

document.querySelector('#search-panel-viewBooks').addEventListener('click', (event) => {
  jichengSearchEngine.filterBooksView();
});

document.addEventListener('DOMContentLoaded', (event) => {
  jichengSearchEngine.init();
});
