var jicheng = {
  /**
   * 常數
   */
  panelFields: [
    'useAncient',
    'useZhu',
    'useShu',
    'useJiao',
    'useNumbering',
    'puncMode',
    'useVertical',
    'useFont',
    'fontSize',
    'useHanzi',
    'tocMode'
  ],
   
  /**
   * 輔助及共用函數
   */
  escapeRegExp(str) {
    // 不脫義 "-"，因 input[pattern] 不允許脫義 "-"
    // Escaping "-" allows the result to be inserted into a character class.
    // Escaping "/" allow the result to be used in a JS regex literal.
    return str.replace(/[/\\^$*+?.|()[\]{}]/g, "\\$&");
  },

  getXPath(node, refNode) {
    const idx = (elem) => {
      let name = elem.nodeName, i = 0;
      while (elem) {
        if (elem.nodeName === name) { i++; }
        elem = elem.previousElementSibling;
      }
      return i;
    };
    const segs = (elem) => {
      if (!elem || elem.nodeType !== 1 || elem === refNode) {
        return [''];
      }
      if (elem.id && document.getElementById(elem.id) === elem) {
        return [`id("${elem.id}")`];
      }
      const s = segs(elem.parentNode);
      s.push(`${elem.nodeName.toLowerCase()}[${idx(elem)}]`);
      return s;
    };
    return segs(node).join('/');
  },

  loadDlKeyValue(dlElem) {
    const getElemValue = (elem) => {
      const dataElem = elem.querySelector('data');
      if (dataElem) {
        return dataElem.value;
      }
      return elem.textContent.trim();
    };

    const processDtDd = (elem) => {
      switch (elem.nodeName.toLowerCase()) {
        case 'dt':
          if (seenDd) {
            groups.push(current);
            current = {name: [], value: []};
            seenDd = false;
          }
          current.name.push(getElemValue(elem));
          break;
        case 'dd':
          current.value.push(getElemValue(elem));
          seenDd = true;
          break;
      }
    };

    const groups = [];
    let current = {name: [], value: []};
    let seenDd = false;
    let child = dlElem.firstChild;
    let grandchild = null;

    while (child) {
      switch (child.nodeName.toLowerCase()) {
        case 'div':
          grandchild = child.firstChild;
          while (grandchild) {
            processDtDd(grandchild);
            grandchild = grandchild.nextSibling;
          }
          break;
        default:
          processDtDd(child);
          break;
      }
      child = child.nextSibling;
    }

    groups.push(current);

    const metadata = {};
    for (let group of groups) {
      for (let n of group.name) {
        metadata[n] = [];
        for (let v of group.value) {
          metadata[n].push(v);
        }
      }
    }

    return metadata;
  },

  xhr(params = {}) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      if (params.onreadystatechange) {
        xhr.onreadystatechange = function (event) {
          params.onreadystatechange(xhr);
        };
      }

      xhr.onload = function (event) {
        if (xhr.status == 200 || xhr.status == 0) {
          // we only care about real loading success
          resolve(xhr);
        } else {
          // treat "404 Not found" or so as error
          let statusText = xhr.statusText || scrapbook.httpStatusText[xhr.status];
          statusText = xhr.status + (statusText ? " " + statusText : "");
          reject(new Error(statusText));
        }
      };

      xhr.onabort = function (event) {
        // resolve with no param
        resolve();
      };

      xhr.onerror = function (event) {
        // No additional useful information can be get from the event object.
        reject(new Error("Network request failed."));
      };

      xhr.ontimeout = function (event) {
        reject(new Error("Request timeout."));
      };

      xhr.responseType = params.responseType;
      xhr.open(params.method || "GET", params.url, true);

      if (params.overrideMimeType) { xhr.overrideMimeType(params.overrideMimeType); }
      if (params.timeout) { xhr.timeout = params.timeout; }

      xhr.send();
    });
  },

  async sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  },

  async forEachAsync(iterable, cb, threshold = 100) {
    let t = new Date();
    let i = 0;
    for (let item of iterable) {
      await cb(item, i++, iterable);
      if (new Date() - t > threshold) {
        await this.sleep(0);
        t = new Date();
      }
    }
  },

  /**
   * 主要函數 / 介面相關
   */
  loadBookMeta(mainElem) {
    const dlElem = mainElem.querySelector('header > dl[class~="元資料"]');
    if (!dlElem) { return {}; }
    return this.loadDlKeyValue(dlElem);
  },

  togglePanel(willShow) {
    if (typeof willShow !== "boolean") {
      willShow = !!panel.hidden;
    }

    if (willShow) {
      panel.hidden = false;
      document.querySelector('#panel-close').focus();
    } else {
      panel.hidden = true;
      this.applyOptions();
      this.saveLocalStorage();
    }
  },

  applyOptions() {
    let useAncient = false;
    if (document.body.getAttribute('data-type') === 'book') {
      // meta 指定非古版者不允許切換為古版
      useAncient = (() => {
        const meta = this.loadBookMeta(jicheng.mainElem);
        if (['false', '0', '非古版'].includes((meta['古版'] || [undefined])[0])) {
          return false;
        }

        let mode = document.querySelector('#useAncient').value;

        switch (mode) {
          case "true":
            return true;
          case "false":
            return false;
          default:
            return false;
        }
      })();
    }

    let useVertical = false;
    {
      let mode = document.querySelector('#useVertical').value;

      switch (mode) {
        case "true":
          useVertical = true;
          break;
        case "false":
          useVertical = false;
          break;
        default:
          // 古版自動使用直書，除非 meta 設定為非直書
          useVertical = (() => {
            const meta = this.loadBookMeta(jicheng.mainElem);
            if (['false', '0', '非直書'].includes((meta['直書'] || [undefined])[0])) {
              return false;
            }
            return !!useAncient;
          })();
          break;
      }
    }

    let fontFamily = false;
    {
      let mode = document.querySelector('#useFont').value;

      switch (mode) {
        case "default":
          fontFamily = false;
          break;
        case "hanamin":
          fontFamily = `HanaMin, serif`;
          break;
        case "twkai":
          fontFamily = `TWKai, sans-serif`;
          break;
        default:
          // @TODO: 根據情況自動判斷
          if (useAncient) {
            // 古版時使用中文字體，因英文字體的標點符號有時不能轉90度排
            fontFamily = `"DroidSansFallback", "WenQuanYi Zen Hei", "Heiti TC", "PMingLiU", "PMingLiU-ExtB", san-serif`;
          } else {
            fontFamily = false;
          }
          break;
      }
    }

    let fontSize = 16;
    {
      let size = document.querySelector('#fontSize').value;
      fontSize = parseInt(size, 10);
    }

    let useZhu = true;
    if (document.body.getAttribute('data-type') === 'book') {
      let mode = document.querySelector('#useZhu').value;

      switch (mode) {
        case "true":
          useZhu = true;
          break;
        case "false":
          useZhu = false;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useZhu = true;
          break;
      }
    }

    let useHanzi = true;
    {
      let mode = document.querySelector('#useHanzi').value;

      switch (mode) {
        case "true":
          useHanzi = true;
          break;
        case "false":
          useHanzi = false;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useHanzi = true;
          break;
      }
    }

    let useShu = true;
    if (document.body.getAttribute('data-type') === 'book') {
      let mode = document.querySelector('#useShu').value;

      switch (mode) {
        case "true":
          useShu = true;
          break;
        case "false":
          useShu = false;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useShu = true;
          break;
      }
    }

    let useJiao = true;
    if (document.body.getAttribute('data-type') === 'book') {
      let mode = document.querySelector('#useJiao').value;

      switch (mode) {
        case "true":
          useJiao = true;
          break;
        case "false":
          useJiao = false;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useJiao = !useAncient;
          break;
      }
    }

    let useNumbering = 'none';
    if (document.body.getAttribute('data-type') === 'book') {
      let mode = document.querySelector('#useNumbering').value;

      switch (mode) {
        case "none":
        case "inline":
        case "hanging":
          useNumbering = mode;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useNumbering = !useAncient ? "hanging" : "none";
          break;
      }
    }

    let puncMode = 'normal';
    if (document.body.getAttribute('data-type') === 'book') {
      let mode = document.querySelector('#puncMode').value;

      switch (mode) {
        case "normal":
        case "removeModern":
          puncMode = mode;
          break;
        default:
          // @TODO: 根據情況自動判斷
          puncMode = "normal";
          break;
      }
    }

    this.setAncientMode(useAncient);
    this.setVerticalMode(useVertical);
    this.setFontMode(fontFamily);
    this.setFontSize(fontSize);
    this.setHanzi(useHanzi);
    this.setZhuMode(useZhu);
    this.setShuMode(useShu);
    this.setJiaoMode(useJiao);
    this.setNumberingMode(useNumbering);
    this.setPuncMode(puncMode);
  },

  /**
   * 切換古版
   */
  setAncientMode(useAncient) {
    const isAncient = document.documentElement.classList.contains('古版');

    if (useAncient) {
      if (!isAncient) {
        document.documentElement.classList.add('古版');
      }
    } else {
      if (isAncient) {
        document.documentElement.classList.remove('古版');
      }
    }
  },

  /**
   * 切換直書模式
   */
  setVerticalMode(useVertical) {
    const handlerModifiers = ['ctrlKey', 'altKey', 'shiftKey', 'metaKey'];

    const keyboardHandler = function (event) {
      if (handlerModifiers.some(x => !!event[x])) { return; }
      if (!panel.hidden) { return; }

      switch (event.key) {
        case 'PageUp':
          window.scrollBy(getPageHeight('X'), 0);
          event.preventDefault();
          break;
        case "PageDown":
          window.scrollBy(-getPageHeight('X'), 0);
          event.preventDefault();
          break;
        case 'End':
          window.scrollTo(-document.documentElement.scrollWidth, 0);
          event.preventDefault();
          break;
        case 'Home':
          window.scrollTo(0, 0);
          event.preventDefault();
          break;
      }
    };

    const getLineHeight = function (elem) {
      return parseInt(window.getComputedStyle(elem, null).lineHeight, 10) || 16;
    };

    const getPageHeight = function (axis) {
      switch (axis) {
        case 'X': {
          return document.documentElement.clientWidth;
        }
        case 'Y':
        default: {
          return document.documentElement.clientHeight;
        }
      }
    };

    // @TODO: allow scrolling a scrollbar in the content
    const mouseWheelHandler = function (event) {
      if (handlerModifiers.some(x => !!event[x])) { return; }
      if (!panel.hidden) { return; }

      let deltaY = event.deltaY;
      switch (event.deltaMode) {
        case WheelEvent.DOM_DELTA_LINE: {
          deltaY *= getLineHeight(event.target);
          break;
        }
        case WheelEvent.DOM_DELTA_PAGE: {
          deltaY *= getPageHeight('X');
          break;
        }
      }

      window.scrollBy(-deltaY, 0);
      event.preventDefault();
    };

    const setVerticalMode = function (useVertical) {
      const isVertical = document.documentElement.classList.contains('直書');

      if (useVertical) {
        if (!isVertical) {
          document.documentElement.classList.add('直書');
          window.addEventListener('keydown', keyboardHandler);
          window.addEventListener('wheel', mouseWheelHandler);
        }
      } else {
        if (isVertical) {
          document.documentElement.classList.remove('直書');
          window.removeEventListener('keydown', keyboardHandler);
          window.removeEventListener('wheel', mouseWheelHandler);
        }
      }
    };

    this.setVerticalMode = setVerticalMode;
    return setVerticalMode(useVertical);
  },

  /**
   * 切換字體
   */
  setFontMode(fontFamily) {
    if (!this.setFontMode.css) {
      const css = this.setFontMode.css = document.createElement('style');
    }

    if (!fontFamily) {
      if (this.setFontMode.css.parentNode) {
        this.setFontMode.css.remove();
      }
    } else {
      if (!this.setFontMode.css.parentNode) {
        document.querySelector('head').appendChild(this.setFontMode.css);
      }
      this.setFontMode.css.textContent = `\
main, #panel-main-toc-list {
  font-family: ${fontFamily};
}`;
    }
  },

  setFontSize(fontSize) {
    if (!this.setFontSize.css) {
      const css = this.setFontSize.css = document.createElement('style');
      document.querySelector('head').appendChild(css);
    }

    this.setFontSize.css.textContent = `html { font-size: ${fontSize}px; }`;
  },

  setHanzi(useHanzi) {
    if (!jchanzi) { return; }

    if (useHanzi) {
      jchanzi.processPage();
    } else {
      jchanzi.unprocessPage();
    }
  },

  /**
   * 切換註、疏、校、編號
   */
  setZhuMode(enable) {
    if (!this.setZhuMode.css) {
      const css = this.setZhuMode.css = document.createElement('style');
      css.textContent = `.註 { display: none; }`;
    }

    if (enable) {
      if (this.setZhuMode.css.parentNode) {
        this.setZhuMode.css.remove();
      }
    } else {
      if (!this.setZhuMode.css.parentNode) {
        document.querySelector('head').appendChild(this.setZhuMode.css);
      }
    }
  },

  setShuMode(enable) {
    if (!this.setShuMode.css) {
      const css = this.setShuMode.css = document.createElement('style');
      css.textContent = `.疏 { display: none; }`;
    }

    if (enable) {
      if (this.setShuMode.css.parentNode) {
        this.setShuMode.css.remove();
      }
    } else {
      if (!this.setShuMode.css.parentNode) {
        document.querySelector('head').appendChild(this.setShuMode.css);
      }
    }
  },

  setJiaoMode(enable) {
    if (!this.setJiaoMode.css) {
      const css = this.setJiaoMode.css = document.createElement('style');
      css.textContent = `.校 { display: none; }`;
    }

    if (enable) {
      if (this.setJiaoMode.css.parentNode) {
        this.setJiaoMode.css.remove();
      }
    } else {
      if (!this.setJiaoMode.css.parentNode) {
        document.querySelector('head').appendChild(this.setJiaoMode.css);
      }
    }
  },

  setNumberingMode(mode) {
    if (!this.setNumberingMode.css) {
      const css = this.setNumberingMode.css = document.createElement('style');
      css.textContent = `.編號 { display: none; }`;

      // 計算編號總數及編號數字所需寬度
      const count = this.mainElem.querySelectorAll('.編號').length;
      const width = 0.5 * (count.toString(10).length + 1);

      const cssHanging = this.setNumberingMode.cssHanging = document.createElement('style');
      cssHanging.textContent = count <= 0 ? "" : `\
html:not(.直書) body > main {
  padding: 0 ${Math.max(width - 1.25, 0)}em;
}

html:not(.直書) .編號 {
  position: absolute;
  margin-top: .1em;
  margin-left: -${width + 0.5}em;
  width: ${width + 0.25}em;
  text-align: right;
}

html.直書 .編號 {
  position: absolute;
  margin-top: -1em;
}
`;
    }

    switch (mode) {
      case "inline": {
        if (this.setNumberingMode.css.parentNode) {
          this.setNumberingMode.css.remove();
        }
        if (this.setNumberingMode.cssHanging.parentNode) {
          this.setNumberingMode.cssHanging.remove();
        }
        break;
      }
      case "hanging": {
        if (this.setNumberingMode.css.parentNode) {
          this.setNumberingMode.css.remove();
        }
        if (!this.setNumberingMode.cssHanging.parentNode) {
          document.querySelector('head').appendChild(this.setNumberingMode.cssHanging);
        }
        break;
      }
      default: {
        if (!this.setNumberingMode.css.parentNode) {
          document.querySelector('head').appendChild(this.setNumberingMode.css);
        }
        if (this.setNumberingMode.cssHanging.parentNode) {
          this.setNumberingMode.cssHanging.remove();
        }
        break;
      }
    }
  },

  setPuncMode(mode) {
    const rPunctuations = /[，、。·．‧；：？！…《》〈〉「」『』‘’“”〝〞‵′]+/i;
    const excludeSelectors = [
      'main > header > dl[class~="元資料"] *',
      'span.punc',
    ];

    const processElem = (elem) => {
      new Mark(elem).markRegExp(rPunctuations, {
        element: 'span',
        className: 'puncs',
        exclude: excludeSelectors,
      });
    };

    if (!this.setPuncMode.css) {
      const css = this.setPuncMode.css = document.createElement('style');
      css.textContent = `.puncs { display: none; }`;
    }

    switch (mode) {
      case "normal": {
        if (this.setPuncMode.css.parentNode) {
          this.setPuncMode.css.remove();
        }
        break;
      }

      case "removeModern": {
        if (!this.setPuncMode.css.parentNode) {
          document.querySelector('head').appendChild(this.setPuncMode.css);
        }

        processElem(this.mainElem);

        if (!this.mainElemShown.hidden) {
          processElem(this.mainElemShown);
        }
        break;
      }
    }
  },

  generateToc() {
    const titleElemsExclude = new Set(this.mainElem.querySelectorAll(
      'header h1, header h2, header h3, header h4, header h5, header h6'
    ));

    const titleElems = Array.from(this.mainElem.querySelectorAll('h1, h2, h3, h4, h5, h6'))
        .filter(x => !titleElemsExclude.has(x));

    const contentElemsMap = this.contentElemsMap = new Map();
    titleElems.forEach((elem) => {
      let curElem = elem;
      while (curElem.parentNode.nodeName.toLowerCase() !== 'main') {
        curElem = curElem.parentNode;
      }
      contentElemsMap.set(elem, curElem);
    });

    const root = document.querySelector('#panel-main-toc-list');
    let prevElem = root;
    let prevLevel = titleElems.reduce((curValue, titleElem) => {
      const level = parseInt(titleElem.nodeName.match(/(\d+)$/)[1], 10);
      return Math.min(curValue, level);
    }, Infinity);

    // 全文
    {
      const ol = document.createElement('ol');
      root.appendChild(ol);
      const li = document.createElement('li');
      ol.appendChild(li);

      const a = document.createElement('a');
      a.href = '#';
      a.textContent = '（全文）';
      a.addEventListener('click', (event) => {
        this.loadSection(null);
      });
      li.appendChild(a);

      prevElem = li;
    }

    // 首節
    {
      const li = document.createElement('li');
      prevElem.parentNode.appendChild(li);

      const a = document.createElement('a');
      a.href = '#t=.';
      a.textContent = '（首節）';
      a.addEventListener('click', (event) => {
        this.loadSection(this.mainElem);
      });
      li.appendChild(a);

      prevElem = li;
    }

    titleElems.forEach((titleElem) => {
      const level = parseInt(titleElem.nodeName.match(/(\d+)$/)[1], 10);
      const li = document.createElement('li');

      if (level > prevLevel) {
        const ol = document.createElement('ol');
        prevElem.appendChild(ol);
        ol.appendChild(li);
      } else if (level < prevLevel) {
        let curElem = prevElem;
        while (prevLevel > level) {
          curElem = curElem.parentNode.parentNode;
          prevLevel--;
        }
        curElem.parentNode.appendChild(li);
      } else {
        prevElem.parentNode.appendChild(li);
      }

      const a = document.createElement('a');
      a.textContent = titleElem.textContent;
      a.href = '#t=' + this.getXPath(titleElem, this.mainElem).slice(1);
      a.addEventListener('click', (event) => {
        this.loadSection(titleElem);
      });
      li.appendChild(a);

      prevElem = li;
      prevLevel = level;
    });
  },

  // titleElem - falsy: 顯示全文, mainElem: 顯示首節, 其他: 顯示該標題元素所在節
  loadSection(titleElem) {
    switch (document.querySelector('#tocMode').value) {
      case 'locate': {
        // 顯示全文
        this.mainElemShown.hidden = true;
        this.mainElem.hidden = false;

        if (titleElem) {
          this.locateElement(titleElem);
        } else {
          window.scrollTo(0, 0);
        }

        this.togglePanel(false);
        return;
      }
      default: {
        break;
      }
    }

    if (!titleElem) {
      // 顯示全文
      this.mainElemShown.hidden = true;
      this.mainElem.hidden = false;
    } else {
      let prevTitleElem = null;
      let nextTitleElem = null;
      let contentElem = null;
      let nextContentElem = null;

      if (titleElem === this.mainElem) {
        // 首節
        contentElem = this.mainElem.firstChild;

        const item = this.contentElemsMap.entries().next();
        if (!item.done) {
          nextTitleElem = item.value[0];
          nextContentElem = item.value[1];
        }
      } else {
        contentElem = this.contentElemsMap.get(titleElem);

        const iter = this.contentElemsMap.entries();
        do {
          item = iter.next();
          if (item.done) { break; }
          if (item.value[0] === titleElem) { break; }
          prevTitleElem = item.value[0];
        } while (true)

        item = iter.next();

        if (!item.done) {
          nextTitleElem = item.value[0];
        }

        while (!item.done) {
          if (item.value[1] !== contentElem) {
            nextContentElem = item.value[1];
            break;
          }

          item = iter.next();
        }
      }

      // 設定要顯示的元素
      {
        this.mainElemShown.innerHTML = '';

        let curElem = contentElem;
        while(curElem && curElem !== nextContentElem) {
          this.mainElemShown.appendChild(curElem.cloneNode(true));
          curElem = curElem.nextSibling;
        }
      }

      // 建立「上一節」「下一節」
      {
        const footerElem = this.mainElemShown.appendChild(document.createElement('footer'));

        if (prevTitleElem) {
          const div = footerElem.appendChild(document.createElement('div'));
          div.textContent = '上一節：';

          const a = div.appendChild(document.createElement('a'));
          a.textContent = prevTitleElem.textContent;
          a.href = '#t=' + this.getXPath(prevTitleElem, this.mainElem).slice(1);
          a.addEventListener('click', (event) => {
            this.loadSection(prevTitleElem);
          });
        } else if (titleElem !== this.mainElem) {
          const div = footerElem.appendChild(document.createElement('div'));
          div.textContent = '上一節：';

          const a = div.appendChild(document.createElement('a'));
          a.textContent = '（首節）';
          a.href = '#t=.';
          a.addEventListener('click', (event) => {
            this.loadSection(this.mainElem);
          });
        } else {
          const div = footerElem.appendChild(document.createElement('div'));
          div.textContent = '上一節：無';
        }

        if (nextTitleElem) {
          const div = footerElem.appendChild(document.createElement('div'));
          div.textContent = '下一節：';

          const a = div.appendChild(document.createElement('a'));
          a.textContent = nextTitleElem.textContent;
          a.href = '#t=' + this.getXPath(nextTitleElem, this.mainElem).slice(1);
          a.addEventListener('click', (event) => {
            this.loadSection(nextTitleElem);
          });
        } else {
          const div = footerElem.appendChild(document.createElement('div'));
          div.textContent = '下一節：無';
        }
      }

      this.mainElem.hidden = true;
      this.mainElemShown.hidden = false;
    }

    window.scrollTo(0, 0);
    this.togglePanel(false);
  },

  // @TODO: 若 target 的 parent 為 pre，Chrome 取得的 domRect 數值會不正確
  locateElement(target) {
    if (typeof target === 'string') {
      const r = document.evaluate(target, this.mainElem, null, XPathResult.ANY_TYPE, null);
      if (!r) { return; }
      target = r.iterateNext();
    }
    if (!target) { return; }

    // target 被隱藏時無法定位，建立暫時節點作為參照 
    const getUppermostHiddenParent = (node) => {
      let curNode = node;
      while (!node.offsetParent) {
        curNode = node;
        node = node.parentNode;
      }
      return curNode;
    };

    let ref = target;
    if (!ref.offsetParent) {
      ref = getUppermostHiddenParent(ref);
      ref = ref.parentNode.insertBefore(document.createElement('span'), ref);
    }

    if (!document.documentElement.classList.contains('直書')) {
      const domRect = ref.getBoundingClientRect();
      window.scrollBy(domRect.left, domRect.top);
    } else {
      const domRect = ref.getBoundingClientRect();
      const viewportWidth = document.documentElement.clientWidth;
      const deltaX = domRect.left + domRect.width - viewportWidth;
      window.scrollBy(deltaX, domRect.top);
    }

    if (ref !== target) {
      ref.remove();
    }
  },

  loadLocalStorage() {
    this.panelFields.forEach((key) => {
      const elem = document.querySelector(`#${key}`);
      if (elem !== null) {
        const value = localStorage.getItem(`jc_${key}`);
        if (value !== null) {
          elem.value = value;
        }
      }
    });
  },

  saveLocalStorage() {
    this.panelFields.forEach((key) => {
      const elem = document.querySelector(`#${key}`);
      if (elem !== null && elem.value !== '') {
        localStorage.setItem(`jc_${key}`, elem.value);
      }
    });
  },

  loadUrlParams() {
    const params = new URLSearchParams(location.hash.replace('#', ''));
    this.panelFields.forEach((key) => {
      if (params.has(key)) {
        const elem = document.querySelector(`#${key}`);
        if (elem !== null) {
          // 若 URL 參數傳來的值不可用則忽略
          const _value = elem.value;
          elem.value = params.get(key);
          if (elem.value === '') {
            elem.value = _value;
          }
        }
      }
    });
  },

  runUrlActions() {
    const params = new URLSearchParams(location.hash.replace('#', ''));

    // target (locate)
    if (params.has('t')) {
      // onload 事件觸發時 getBoundingClientRect() 未能取得正確座標
      setTimeout(() => {
        this.locateElement(params.get('t'));
      }, 0);
    }

    // highlight
    if (params.has('hl')) {
      let regex;
      try {
        const regexText = params.get('hl');
        /^\/(.*)\/([^/]*?)$/.test(regexText);
        regex = new RegExp(RegExp.$1, RegExp.$2);
      } catch (ex) {
        console.error(ex);
      }

      if (regex) {
        new Mark(jicheng.mainElem).markRegExp(regex, {
          acrossElements: true,
        });
      }
    }
  },
};

document.addEventListener('DOMContentLoaded', (event) => {
  jicheng.panelElem = document.querySelector('#panel');
  jicheng.mainElem = document.querySelector('main');

  jicheng.mainElemShown = document.createElement('main');
  jicheng.mainElemShown.hidden = true;
  jicheng.mainElem.parentNode.insertBefore(jicheng.mainElemShown, jicheng.mainElem.nextSibling);

  var elem = document.querySelector('#panel-open-wrapper');
  if (elem) {
    elem.hidden = false;
  }

  var elem = document.querySelector('#panel-open');
  if (elem) {
    elem.href = "javascript:";
    elem.addEventListener('click', (event) => {
      jicheng.togglePanel(true);
    });
  }

  var elem = document.querySelector('#panel-close');
  if (elem) {
    elem.href = "javascript:";
    elem.addEventListener('click', (event) => {
      jicheng.togglePanel(false);
    });
  }

  if (document.body.getAttribute('data-type') !== 'book') {
    var elem = document.querySelector('#panel-main-view');
    if (elem) {
      elem.hidden = true;
    }
  }

  jicheng.loadLocalStorage();
  jicheng.loadUrlParams();
  jicheng.generateToc();
  jicheng.applyOptions();
});

window.addEventListener('load', (event) => {
  jicheng.runUrlActions();
});
